import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { withStyles } from '@material-ui/core/styles';

import RegisterCustomer from './components/RegisterCustomer';
import RegisterProvider from './components/RegisterProvider';
import RegisterProduct from './components/RegisterProduct';
import './App.css';

import axios from 'axios';

const styles = theme => ({
  layoutApp: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'scretch'
  },
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

class App extends Component {

  state = {
    loading: false,
    customers: [],
    products: [],
    providers: [],
  }

  async componentDidMount() {
    const results = await this.getRecords();

    const [ providers, customers, products ] = results;
    this.setState({ providers, customers, products });
  }

  handleClickOpen = () => {
    this.setState({ loading: true });
  };

  handleClose = () => {
    this.setState({ loading: false });
  };

  getRecords() {

    const endpoints = [
      '/Providers/',
      '/Customers/',
      '/Products/'
    ]

    const BASE_URL = process.env.REACT_APP_BASE_URL;
    const promises = endpoints.map( endpoint => axios.get( BASE_URL + endpoint ));
    return Promise.all(promises).then( results => results.map( result => result.data ) );

  }

  render() {

    const { classes } = this.props;

    return (
      <div className="App">
        
        <div className={classes.layoutApp}>
          <RegisterCustomer onSubmitted={this.handleClickOpen}/>
          <RegisterProvider onSubmitted={this.handleClickOpen}/>
          <RegisterProduct onSubmitted={this.handleClickOpen}/>
        </div>

        <div>
          <h1>Clientes</h1>
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Nombre del cliente</TableCell>
                  <TableCell>Apellido(s) del cliente</TableCell>
                  <TableCell>Domicilio</TableCell>
                  <TableCell>Teléfono</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.customers.map(row => {
                  return (
                    <TableRow key={row.id}>
                      <TableCell>{row.name}</TableCell>
                      <TableCell>{row.lastName}</TableCell>
                      <TableCell>{row.address}</TableCell>
                      <TableCell>{row.phone}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Paper>
        </div>

        <div>
          <h1>Proveedores</h1>
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Nombre del proveedor</TableCell>
                  <TableCell>Apellido(s) del proveedor</TableCell>
                  <TableCell>Compañía</TableCell>
                  <TableCell>Domicilio</TableCell>
                  <TableCell>Teléfono</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.providers.map(row => {
                  return (
                    <TableRow key={row.id}>
                      <TableCell>{row.name}</TableCell>
                      <TableCell>{row.lastName}</TableCell>
                      <TableCell>{row.company}</TableCell>
                      <TableCell>{row.companyAddress}</TableCell>
                      <TableCell>{row.companyPhone}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Paper>
        </div>

        <div>
          <h1>Productos/Medicinas</h1>
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Nombre del producto/medicina</TableCell>
                  <TableCell>Existencias</TableCell>
                  <TableCell>Precio</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.products.map(row => {
                  return (
                    <TableRow key={row.id}>
                      <TableCell>{row.name}</TableCell>
                      <TableCell>{row.stockNumber}</TableCell>
                      <TableCell>{row.price}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Paper>
        </div>

        <Dialog
          open={this.state.loading}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Aviso"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Datos registrados correctamente.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              Aceptar
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(App);


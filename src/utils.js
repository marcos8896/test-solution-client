
export function handleChange(name, event) {
  this.setState({
    [name]: event.target.value,
  });
}
import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import classNames from 'classnames';
import axios from 'axios';

import { handleChange } from '../utils';

const styles = theme => ({
  card: {
    width: 275,
    margin: '25px'
  },
  title: {
    fontSize: 20,
  },
  pos: {
    marginBottom: 12,
  },
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

class RegisterProduct extends Component {

  state = {
    name: '',
    stockNumber: '',
    price: '',
  }

  clearState = () => {
    this.setState({
      name: '',
      stockNumber: '',
      price: '',
    })
  }

  onSubmitProduct( event ) {

    event.preventDefault();

    const url = process.env.REACT_APP_BASE_URL +  '/Products/';

    const data = { ...this.state };

    axios.post( url, data)
      .then( response => {
        this.clearState();
        if ( response.status === 200 ) {
              this.props.onSubmitted();
          } else 
            this.props.onSubmitted();
          
      })
      .catch( err => {
        console.log('err: ', err);
        this.clearState();
      });
  }

  render() {

    const { classes } = this.props;

    return (
      <div className="RegisterProduct">
        <Card className={classes.card}>
        <Typography className={classes.title} color="textSecondary">
          Registrar producto
        </Typography>
        <CardContent>

          <form 
            noValidate autoComplete="off">

            <TextField
              label="Ingresar nombre del prod."
              className={classes.textField}
              margin="normal"
              value={this.state.name}
              onChange={handleChange.bind(this, 'name')}
            />

            <TextField
              label="Ingresar existencias"
              className={classes.textField}
              margin="normal"
              type="number"
              value={this.state.stockNumber}
              onChange={handleChange.bind(this, 'stockNumber')}
            />
            
            <TextField
              label="Ingresar precio"
              className={classes.textField}
              margin="normal"
              type="number"
              value={this.state.price}
              onChange={handleChange.bind(this, 'price')}
            />

            <Button 
              variant="contained" 
              size="small" 
              className={classes.button}
              onClick={this.onSubmitProduct.bind(this)} 
            >
              <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
              Guardar
            </Button>

          </form>

        </CardContent>
      </Card>
      </div>
    );
  }
}

export default withStyles(styles)(RegisterProduct);

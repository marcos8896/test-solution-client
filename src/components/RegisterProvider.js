import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import classNames from 'classnames';
import axios from 'axios';

import { handleChange } from '../utils';

const styles = theme => ({
  card: {
    width: 275,
    margin: '25px'
  },
  title: {
    fontSize: 20,
  },
  pos: {
    marginBottom: 12,
  },
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

class RegisterProvider extends Component {

  state = {
    name: '',
    lastName: '',
    company: '',
    companyAddress: '',
    companyPhone: ''
  }

  clearState = () => {
    this.setState({
      name: '',
      lastName: '',
      company: '',
      companyAddress: '',
      companyPhone: ''
    })
  }

  onSubmitProvider( event ) {

    event.preventDefault();

    const url = process.env.REACT_APP_BASE_URL +  '/Providers/';

    const data = { ...this.state };

    axios.post( url, data)
      .then( response => {
        this.clearState();
        if ( response.status === 200 ) {
              this.props.onSubmitted();
          } else 
            this.props.onSubmitted();
          
      })
      .catch( err => {
        console.log('err: ', err);
        this.clearState();
      });
  }

  render() {

    const { classes } = this.props;

    return (
      <div className="RegisterProvider">
        <Card className={classes.card}>
        <Typography className={classes.title} color="textSecondary">
          Registrar proveedor
        </Typography>
        <CardContent>

          <form 
            noValidate autoComplete="off">

            <TextField
              label="Ingresar nombre"
              className={classes.textField}
              margin="normal"
              value={this.state.name}
              onChange={handleChange.bind(this, 'name')}
            />

            <TextField
              label="Ingresar apellidos"
              className={classes.textField}
              margin="normal"
              value={this.state.lastName}
              onChange={handleChange.bind(this, 'lastName')}
            />
            
            <TextField
              label="Ingresar compañía"
              className={classes.textField}
              margin="normal"
              value={this.state.company}
              onChange={handleChange.bind(this, 'company')}
            />

            <TextField
              label="Ingresar domicilio"
              className={classes.textField}
              margin="normal"
              value={this.state.companyAddress}
              onChange={handleChange.bind(this, 'companyAddress')}
            />

            <TextField
              label="Ingresar teléfono"
              className={classes.textField}
              margin="normal"
              value={this.state.companyPhone}
              onChange={handleChange.bind(this, 'companyPhone')}
            />

            <Button 
              variant="contained" 
              size="small" 
              className={classes.button}
              onClick={this.onSubmitProvider.bind(this)} 
            >
              <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
              Guardar
            </Button>

          </form>

        </CardContent>
      </Card>
      </div>
    );
  }
}

export default withStyles(styles)(RegisterProvider);
